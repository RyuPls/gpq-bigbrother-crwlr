# -*- coding: utf-8 -*-
import scrapy


class ExampleSpider(scrapy.Spider):
    name = "example"
    allowed_domains = ["scrapy.org"]
    start_urls = (
        'http://www.scrapy.org/',
    )

    def parse(self, response):
        pass
