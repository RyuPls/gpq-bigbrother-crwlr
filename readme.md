#CRWLR

Crawler based on Scrapy (http://scrapy.org/), coded in Python 2.7.9.

It crawl by default *www.liteweb.fr* because it is their I followed the tutorial for Scrapy. (Link to the tuto in the 'Help and Links' section)

##Getting Started

In the root folder of the project :

``` Terminal
scrapy crawl crwlr
```

scrapy : name of the command
crawl : action to make
crwlr : name of our crawler 

##Installation...
### of Scrapy

First we have to install Scrapy, check that you have Python and pip installed.
In a terminal :
``` Terminal
python --version
pip --version
```

To install Scrapy : 
``` Terminal
pip install Scrapy
```

### of CRWLR

Just clone the project !

## To Do

The generation of the csv file doesn't work !

## Help and Links

Tutorial on Scrapy : http://www.liteweb.fr/2012/06/12/scrapy-loutil-de-crawl-facile-pour-extraire-des-donnees.html

The file **crwlr/spiders/spider.py** contain the crawler.

The file **crwlr/settings.py** contain the settings.