from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor

from crwlr.items import CrwlrItem

class MyCrawlerSpider(CrawlSpider):
    # nom du crawler 
    name = 'crwlr'

    # domaine(s) sur le ou lesquels le crawler aura le droit d'aller
    allowed_domains = ['www.liteweb.fr']

    # point de depart : vous pouvez mettre une liste d'url, separees par des virgules
    start_urls = ['http://www.liteweb.fr']

    # tres important : les regles en fonction du schema des urls (c'est des regexp)
    rules = [
        # on autorise /article et que l'on va parser avec la methode parse_item, et que l'on va aussi suivre pour extraire des liens
        Rule(SgmlLinkExtractor(allow=['\/article\/']), callback='parse_item',follow='True'),

        # on autorise le crawler a parcourir et a extraire des liens de tous les .html, mais pas si y'a /about dans l'url
        Rule(SgmlLinkExtractor(allow=['\.html'],deny=['\/about']), follow='True')
    ]

    def parse_item(self, response):
        hxs = HtmlXPathSelector(response)
        item = CrwlrItem()
        item['title'] = hxs.select("//h1/text()").extract()
        item['url']= response.url

        # exemples de scrap plus complexes avec des regexp :
        # item['datereview'] = mtabletd.select('./tr[position()=2]/td[position()=3]/strong/text()').re(r'em (.*)$')
        # item['note'] = mtabletd.select('./tr[position()=2]/td/table/tr/td[position()=3]/img/@src').re(r'avloja_m([0-9]+).gif')
        # item['textreview'] = mtabletd.select('./tr[position()=4]/td/div/p/span[@class="opncomp"]/text()').extract()

        return item